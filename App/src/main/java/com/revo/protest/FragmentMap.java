package com.revo.protest;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.PushService;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Ahmed on 12/18/13.
 */
public class FragmentMap extends Fragment implements GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {
    GoogleMap googleMap;
    double lat;
    double lng;
    LocationClient mLocationClient;
    Location mCurrentLocation;
    Activity context;
    private final static int
            CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    // Milliseconds per second
    private static final int MILLISECONDS_PER_SECOND = 1000;
    // Update frequency in seconds
    public static final int UPDATE_INTERVAL_IN_SECONDS = 10;
    // Update frequency in milliseconds
    private static final long UPDATE_INTERVAL =
            MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    // The fastest update frequency, in seconds
    private static final int FASTEST_INTERVAL_IN_SECONDS = 5;
    // A fast frequency ceiling in milliseconds
    private static final long FASTEST_INTERVAL =
            MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;

    LocationRequest mLocationRequest;


    SharedPreferences mPrefs;
    SharedPreferences.Editor mEditor;
    boolean mUpdatesRequested = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();

//        // Create the LocationRequest object
//        mLocationRequest = LocationRequest.create();
//        // Use high accuracy
//        mLocationRequest.setPriority(
//                LocationRequest.PRIORITY_HIGH_ACCURACY);
//        // Set the update interval to 5 seconds
//        mLocationRequest.setInterval(UPDATE_INTERVAL);
//        // Set the fastest update interval to 1 second
//        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
//
//        mPrefs = context.getSharedPreferences("SharedPreferences",
//                Context.MODE_PRIVATE);
//        // Get a SharedPreferences editor
//        mEditor = mPrefs.edit();
//        /*
//         * Create a new location client, using the enclosing class to
//         * handle callbacks.
//         */
//        mLocationClient = new LocationClient(context, this, this);
//        // Start with updates turned off
//        mUpdatesRequested = true;

    }

    @Override
    public void onPause() {
        // Save the current setting for updates
//        mEditor.putBoolean("KEY_UPDATES_ON", mUpdatesRequested);
//        mEditor.commit();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
//        /*
//         * Get any previous setting for location updates
//         * Gets "false" if an error occurs
//         */
//        if (mPrefs.contains("KEY_UPDATES_ON")) {
//            mUpdatesRequested =
//                    mPrefs.getBoolean("KEY_UPDATES_ON", false);
//
//            // Otherwise, turn off location updates
//        } else {
//            mEditor.putBoolean("KEY_UPDATES_ON", false);
//            mEditor.commit();
//        }
    }

    @Override
    public void onStart() {
        super.onStart();
        //mLocationClient.connect();

    }

    @Override
    public void onStop() {
//        if (mLocationClient.isConnected()) {
//            mLocationClient.removeLocationUpdates(this);
//        }
//        mLocationClient.disconnect();

        super.onStop();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        Button saveButton= (Button) rootView.findViewById(R.id.btn_save);
        PushService.subscribe(context, "Locations", getActivity().getClass());
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONArray locationJsonArray=new JSONArray();
                for (int i=0;i<locationArrayList.size();i++)
                {
                    locationJsonArray.put(locationArrayList.get(i));
                }
                currentUser.put("locationList", locationJsonArray);
                currentUser.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        ParsePush push = new ParsePush();
                        push.setChannel("Locations");
                        push.setMessage("updated");
                        push.sendInBackground();
                    }
                });
            }
        });
        return rootView;
    }

    public void updateMarkers(Location location)
    {
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        MarkerOptions marker = new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude()))
                .title("");
        googleMap.addMarker(marker);

        JSONObject locationObject=new JSONObject();
        try {
            locationObject.put("lat",location.getLatitude());
            locationObject.put("lng",location.getLongitude());
            locationArrayList.add(locationObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }


    public void initializeMap(Location location) {
        if (googleMap == null) {
            Fragment fragment = this.getFragmentManager().findFragmentById(R.id.map);

            googleMap = ((SupportMapFragment) fragment).getMap();
        }
        googleMap.setMyLocationEnabled(true); // false to disable
        //mCurrentLocation = mLocationClient.getLastLocation();
        lat = location.getLatitude();
        lng = location.getLongitude();
        LatLng latLng = new LatLng(lat, lng);

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 10);

        //googleMap.getUiSettings().setMyLocationButtonEnabled(true);

        googleMap.animateCamera(cameraUpdate);


    }


    @Override
    public void onConnected(Bundle dataBundle) {
        // Display the connection status
        Toast.makeText(context, "Connected", Toast.LENGTH_SHORT).show();

        mLocationClient.requestLocationUpdates(mLocationRequest, this);
        locationArrayList = new ArrayList<JSONObject>();
        currentUser = ParseUser.getCurrentUser();

        try {

            // Loading map
            //initializeMap();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDisconnected() {
        // Display the connection status
        Toast.makeText(context, "Disconnected. Please re-connect.",
                Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
 /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        context,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                    connectionResult.getErrorCode(),
                    context,
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);
        }
    }

    ArrayList<JSONObject> locationArrayList;
    ParseUser currentUser;

    @Override
    public void onLocationChanged(Location location) {
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        MarkerOptions marker = new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude()))
                .title("");
        googleMap.addMarker(marker);

        JSONObject locationObject=new JSONObject();
        try {
            locationObject.put("lat",location.getLatitude());
            locationObject.put("lng",location.getLongitude());
            locationArrayList.add(locationObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Decide what to do based on the original request code
        switch (requestCode) {
            case CONNECTION_FAILURE_RESOLUTION_REQUEST:
            /*
             * If the result code is Activity.RESULT_OK, try
             * to connect again
             */
                switch (resultCode) {
                    case Activity.RESULT_OK:
                    /*
                     * Try the request again
                     */
                        break;
                }
        }
    }
}
